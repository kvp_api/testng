package test.tests.w3schools_test;

import com.codeborne.selenide.Selenide;
import com.example.logic.w3schools.W3schoolLogic;
import io.qameta.allure.*;
import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Epic("W3schools задания")
public class W3schoolsTest extends W3schoolLogic {

    @BeforeMethod
    public void setUp(ITestContext context) {
        Selenide.open("https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all");
    }

    @Severity(SeverityLevel.BLOCKER)
    @Feature(value = "Метод позитивного сценария")
    @Parameters({"TmsLink", "taskNumber-param"})
    @Test
    public void executeTask(@Optional("НУЖЕН НОМЕР TmsLink") String tmsLink, Integer taskNumber) {
        Allure.tms(tmsLink, getTMSLink(tmsLink));
        handleTask(taskNumber);
    }
}
