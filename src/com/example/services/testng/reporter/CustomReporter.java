package com.example.services.testng.reporter;

import com.example.services.testng.reporter.data.SuiteResultData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.xml.XmlSuite;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomReporter implements IReporter {
    private static final Logger logger = LoggerFactory.getLogger(CustomReporter.class);

    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
                               String outputDirectory) {

        int totalCount = 0;
        int totalSuccess = 0;
        int totalFailed = 0;
        int totalSkipped = 0;

        HashMap<String, SuiteResultData> allData = new HashMap<>();
        //Iterating over each suite included in the test
        for (ISuite suite : suites) {
            //Following code gets the suite name
            String suiteName = suite.getName();

            //Getting the results for the said suite
            Map<String, ISuiteResult> suiteResults = suite.getResults();
            for (ISuiteResult sr : suiteResults.values()) {
                ITestContext tc = sr.getTestContext();

                int passedSuiteSize = tc.getPassedTests().getAllResults().size();
                int failedSuiteSize = tc.getFailedTests().getAllResults().size();
                int skippedSuiteSize = tc.getSkippedTests().getAllResults().size();
                int suiteSize = passedSuiteSize + failedSuiteSize + skippedSuiteSize;

                totalCount += suiteSize;
                totalSuccess += passedSuiteSize;
                totalFailed += failedSuiteSize;
                totalSkipped += skippedSuiteSize;

                if (allData.containsKey(suiteName)) {
                    SuiteResultData suitedata = allData.get(suiteName);
                    suitedata.addSuccess(passedSuiteSize);
                    suitedata.addFailed(failedSuiteSize);
                    suitedata.addSkipped(skippedSuiteSize);
                } else {
                    SuiteResultData suitedata = new SuiteResultData(passedSuiteSize, failedSuiteSize, skippedSuiteSize);
                    allData.put(suiteName, suitedata);
                }
            }
        }

        String totalReport = "Подробная статистика по наборам:\n";
        String jiraTotalReport = "Подробная статистика по наборам:\n||Тестовый набор||Успешно пройдено||Провалено||Пропущено||\n";
        StringBuilder totalsb = new StringBuilder(totalReport);
        StringBuilder jirasb = new StringBuilder(jiraTotalReport);
        for (Map.Entry<String, SuiteResultData> e : allData.entrySet()) {
            jirasb.append(String.format("|%s|%s|%s|%d|\n",
                    String.format("{color:%s}_%s_{color}", e.getValue().getFailed() > 0 ? "red" : "olive", e.getKey()),
                    String.format("{color:olive}%d{color}", e.getValue().getSuccess()),
                    String.format("{color:red}%d{color}", e.getValue().getFailed()),
                    e.getValue().getSkipped()));

            totalsb.append(String.format("%s : Успешно: %d, Провалено: %d, Пропущено: %d\n",
                    e.getKey(),
                    e.getValue().getSuccess(),
                    e.getValue().getFailed(),
                    e.getValue().getSkipped()));
        }

        double successPercent = (totalSuccess * 100.0) / totalCount;
        double failedPercent = (totalFailed * 100.0) / totalCount;

        String common = String.format("Результаты прогона :\n" +
                        "Всего тестов: %d, Успешно: %d (%f%%), Провалено: %d (%f%%), Пропущено: %d",
                totalCount, totalSuccess, successPercent, totalFailed, failedPercent, totalSkipped);
        totalReport = totalsb.toString();
        jiraTotalReport = jirasb.toString();
        logger.info(common + "\n" + totalReport);
    }
}

