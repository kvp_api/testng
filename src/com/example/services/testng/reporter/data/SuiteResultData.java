package com.example.services.testng.reporter.data;

public class SuiteResultData {
    private int success = 0;
    private int failed = 0;
    private int skipped = 0;

    public int getSuccess() {
        return success;
    }

    public void addSuccess(int success) {
        this.success += success;
    }

    public int getFailed() {
        return failed;
    }

    public void addFailed(int failed) {
        this.failed += failed;
    }

    public int getSkipped() {
        return skipped;
    }

    public void addSkipped(int skipped) {
        this.skipped += skipped;
    }

    public SuiteResultData(int success, int failed, int skipped) {
        this.success = success;
        this.failed = failed;
        this.skipped = skipped;
    }
}
