package com.example.services.testng.listener;

import com.codeborne.selenide.Configuration;
import lombok.SneakyThrows;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IExecutionListener;
import org.testng.ITestContext;
import org.testng.ITestListener;

import java.io.FileReader;
import java.net.InetAddress;
import java.util.Properties;
import java.util.logging.Level;

public class CustomListener implements IExecutionListener, ITestListener {
    private static final Logger logger = LoggerFactory.getLogger(CustomListener.class);

    /**
     * IExecutionListener : onExecutionStart и onExecutionFinish
     */
    @Override
    public void onExecutionFinish() {
        logger.debug("onExecutionFinish");
    }

    @SneakyThrows
    @Override
    public void onExecutionStart() {
        fillBaseRestAssuredAndSeleniumConfig();
        InetAddress ip = InetAddress.getLocalHost();
        logger.info("Network IP address : " + ip);
        logger.debug("onExecutionStart");
    }

    /**
     * Настройки RestAssured и Selenium
     */
    @SneakyThrows
    private void fillBaseRestAssuredAndSeleniumConfig() {
        Properties properties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        properties.load(new FileReader(classLoader.getResource("environment.properties").getFile()));

        Configuration.driverManagerEnabled = Boolean.parseBoolean(properties.getProperty("Configuration.driverManagerEnabled"));
        Configuration.remote = properties.getProperty("Configuration.remote");
        Configuration.reportsFolder = properties.getProperty("Configuration.reportsFolder");
        Configuration.timeout = Long.parseLong(properties.getProperty("Configuration.timeout"));

        /**
         * Для локального запуска, не через селеноид
         */
        //System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");

        /**
         * Для тестирования мобильной версии
         */
        //System.setProperty("chromeoptions.mobileEmulation", "deviceName=Nexus 5");

        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        logPrefs.enable(LogType.CLIENT, Level.ALL);
        logPrefs.enable(LogType.DRIVER, Level.ALL);
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        logPrefs.enable(LogType.PROFILER, Level.ALL);
        logPrefs.enable(LogType.SERVER, Level.ALL);
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("goog:loggingPrefs", logPrefs);
        Configuration.browserCapabilities = capabilities;
    }

    /**
     * ITestListener : onStart
     *
     * @param context
     */
    @Override
    public void onStart(ITestContext context) {
        logger.debug("onStart executed");
    }
}
