package com.example.services.base_test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;

import static com.example.services.allure.AllureLogger.logToAllure;

public class BaseTest {
    protected ITestContext context;
    private String allureTmsLink = "https://example.org/secure/Tests.jspa#/testCase/";
    private String tmsName;
    private static final Logger testLogger = LoggerFactory.getLogger(BaseTest.class);

    public BaseTest() {
    }

    public String getTMSLink(String testCase) {
        this.tmsName = testCase;
        return allureTmsLink + testCase;
    }

    @BeforeTest
    public void beforeTest(ITestContext context) {
        init(context);
    }

    private void init(ITestContext context) {
        this.context = context;
    }

    @AfterMethod
    public void afterMethodResults(ITestResult testResult) {
        defaultAfterMethodResults(testResult);
    }

    public void defaultAfterMethodResults(ITestResult testResult) {
        switch (testResult.getStatus()) {
            case ITestResult.SUCCESS:
                logToAllure("======ТЕСТ ПРОЙДЕН=====");
                break;
            case ITestResult.FAILURE:
                logToAllure("======Ошибка=====");
                break;
            case ITestResult.SKIP:
                logToAllure("======ПРОПУЩЕНО=====");
                break;
            default:
                throw new RuntimeException("Неправильный статус теста");
        }
        logToAllure("Тест завершен!");
    }
}
