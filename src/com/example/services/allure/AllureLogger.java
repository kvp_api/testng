package com.example.services.allure;

import com.codeborne.selenide.Selenide;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import lombok.SneakyThrows;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Base64;

/**
 * Класс содержит методы по работе с системой Allure
 */

public class AllureLogger {

    private static final Logger logger = LoggerFactory.getLogger(AllureLogger.class);

    /**
     * Аннотацию @Step используется для регистрации данного сообщения журнала в Allure.
     *
     * @param message сообщение для входа в отчет allure
     */
    @Step("{0}")
    public static void logToAllure(String message) {
        logger.debug("Logged to allure: " + message);
    }

    @Attachment(value = "Screenshot {str}", type = "image/png")
    public static byte[] getScreenshot(String str) {
        String screenshotAsBase64 = Selenide.screenshot(OutputType.BASE64);
        byte[] decoded = Base64.getDecoder().decode(screenshotAsBase64);
        return decoded;
    }

    /**
     * Выводит красивый json-ответа
     */
    @SneakyThrows
    @Attachment(value = "JSON файл {str1}", type = "application/json", fileExtension = ".json")
    public static byte[] AllureAttachJSONAnswer(String str1, JSONObject jsonObject) {
        return beautifyJson(jsonObject.toString()).getBytes("utf-8");
    }

    /**
     * Выводит красивый xml-ответа
     */
    @SneakyThrows
    @Attachment(value = "XML файл {str1}", type = "application/xml", fileExtension = ".xml")
    public static byte[] AllureAttachXMLAnswer(String str1, String string) {
        return beautifyXml(string).getBytes("utf-8");
    }

    @SneakyThrows
    @Attachment(value = "JSON файл {str1}", type = "application/json", fileExtension = ".json")
    public static byte[] AllureAttachJSONString(String str1, String string) {
        return string.getBytes("utf-8");
    }

    @SneakyThrows
    @Attachment(value = "XML файл {str1}", type = "application/xml", fileExtension = ".xml")
    public static byte[] AllureAttachXMLString(String str1, String string) {
        return string.getBytes("utf-8");
    }

    @SneakyThrows
    public static String beautifyJson(String json) {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = mapper.readValue(json, Object.class);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
    }

    @SneakyThrows
    public static String beautifyXml(String xml) {
        Source xmlInput = new StreamSource(new StringReader(xml));
        StringWriter stringWriter = new StringWriter();
        StreamResult xmlOutput = new StreamResult(stringWriter);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute("indent-number", 2);
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(xmlInput, xmlOutput);
        return xmlOutput.getWriter().toString();
    }
}