package com.example.logic.w3schools.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class MainPage {
    private final SelenideElement buttonRun = $(By.xpath("/html/body/div[2]/div/div[1]/div[1]/button"));

    @Step("Заполнение поля SQL Statement")
    public void fillQueryText(String text) {
        Selenide.executeJavaScript(String.format("window.editor.setValue(\"%s\")", text));
    }

    @Step("Нажать RUN SQL")
    public void clickButton() {
        buttonRun.click();
        sleep(1000);
    }

    @Step("Прочитать Result")
    public ElementsCollection getRowResults() {
        ElementsCollection result = $$(By.xpath("//*[@id=\"divResultSQL\"]/div/table/tbody/tr"));
        return result;
    }

    @Step("Прочитать Result по ContactName")
    public ElementsCollection getContactNameRowResults() {
        ElementsCollection result = $$(By.xpath("//*[@id=\"divResultSQL\"]/div/table/tbody/tr[*]/td[3]"));
        return result;
    }

    /**
     * @param index
     * @param column (1 CustomerID, 2 CustomerName,	3 ContactName, 4 Address, 5 City, 6 PostalCode, 7 Country
     * @return
     */
    @Step("Прочитать значение из записи из необходимой колонки")
    public SelenideElement getValueFromTable(int index, int column) {
        return $(By.xpath(String.format("//*[@id=\"divResultSQL\"]/div/table/tbody/tr[%d]/td[%d]", index + 2, column)));
    }
}


