package com.example.logic.w3schools;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.example.logic.w3schools.pages.MainPage;
import com.example.services.base_test.BaseTest;
import io.qameta.allure.Step;
import org.testng.Assert;

import static com.example.services.allure.AllureLogger.logToAllure;

public class W3schoolLogic extends BaseTest {
    public final MainPage mainPage = new MainPage();

    public void handleTask(Integer numberTask) {
        switch(numberTask) {
            case 1: handleFirstTask(); break;
            case 2: handleSecondTask(); break;
            case 3: handle3Task(); break;
            case 4: handle4Task(); break;
            case 5: handle5Task(); break;
            default:
                break;
        }
    }

    @Step("Выполнение проверок задачи 1")
    private void handleFirstTask() {
        mainPage.fillQueryText("SELECT * FROM Customers;");
        mainPage.clickButton();

        ElementsCollection customerNameList = mainPage.getContactNameRowResults();
        SelenideElement searchableName = customerNameList.findBy(Condition.text("Giovanni Rovelli"));
        int index = customerNameList.indexOf(searchableName);

        SelenideElement checkedAddress = mainPage.getValueFromTable(index, 4);
        Assert.assertEquals(checkedAddress.getText(), "Via Ludovico il Moro 22");
    }

    @Step("Выполнение проверок задачи 2")
    private void handleSecondTask() {
        mainPage.fillQueryText("SELECT * FROM Customers WHERE City = 'London';");
        mainPage.clickButton();
        ElementsCollection result = mainPage.getRowResults();
        Assert.assertTrue(result.size() - 1 == 6);
    }

    @Step("Выполнение проверок задачи 3")
    private void handle3Task() {
        mainPage.fillQueryText("INSERT INTO Customers ('CustomerName', 'ContactName', 'Address', 'City', 'PostalCode', 'Country') VALUES " +
                "('Tester','SUPERTester', 'Lenina 123', 'Protvino', '123 123', 'RU');");
        mainPage.clickButton();
        mainPage.fillQueryText("SELECT * FROM Customers;");
        mainPage.clickButton();
        Assert.assertNotNull(mainPage.getRowResults().findBy(Condition.text("Tester")));
    }

    @Step("Выполнение проверок задачи 4")
    private void handle4Task() {
        mainPage.fillQueryText("UPDATE Customers SET CustomerName = 'Tester123', ContactName = 'SUPERTesterAfterUpdate', Address = 'Podolskaya ulica', City = 'Msk', PostalCode = '321 321', Country = 'CY' WHERE CustomerName = 'Tester';");
        mainPage.clickButton();
        mainPage.fillQueryText("SELECT * FROM Customers;");
        mainPage.clickButton();
        SelenideElement checked = mainPage.getRowResults().findBy(Condition.text("Tester123"));
        logToAllure(checked.getText());
        boolean existCorrectAddress = checked.getText().contains("Podolskaya ulica");
        Assert.assertTrue(existCorrectAddress);
    }

    @Step("Выполнение проверок задачи 5")
    private void handle5Task() {
        mainPage.fillQueryText("DELETE FROM Customers WHERE CustomerName = 'Tester123';");
        mainPage.clickButton();
        ElementsCollection result = mainPage.getRowResults();
        result.findBy(Condition.text("Tester123")).shouldNotBe(Condition.exist);
    }
}
